﻿// <copyright file="SqlRepository.cs" company="Leaderboard Company">
//     Copyright © Leaderboard Company 2016
// </copyright>

namespace Leaderboard.Core
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using Dapper;

    /// <summary>
    /// Repository that stores data in a SQL Server database.
    /// </summary>
    public class SqlRepository : IRepository
    {
        private readonly string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlRepository"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public SqlRepository(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("Connection string cannot be empty", nameof(connectionString));
            }

            this.connectionString = connectionString;
        }

        /// <inheritdoc/>
        public ICollection<Player> GetTopPlayers()
        {
            using (var sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();
                var players = sqlConnection.Query<Player>("SELECT TOP 100 * FROM Players WITH(NOLOCK) ORDER BY Score DESC").ToList();
                SpecifyTimestampKind(players);
                return players;
            }
        }

        /// <inheritdoc/>
        public void UpdatePlayerScore(long playerId, long score, DateTime updatedDateTime)
        {
            var sqlParams = new
            {
                Id = playerId,
                Score = score,
                Timestamp = updatedDateTime.ToUniversalTime()
            };

            using (var sqlConnection = new SqlConnection(this.connectionString))
            {
                sqlConnection.Open();

                try
                {
                    sqlConnection.Execute(@"INSERT INTO Players(Id, Score, Timestamp) VALUES (@Id, @Score, @Timestamp)", sqlParams);
                }
                catch (SqlException e) when (IsPrimaryKeyViolation(e))
                {
                    sqlConnection.Execute(@"UPDATE Players SET Score = Score + @Score WHERE Id = @Id", sqlParams);
                    sqlConnection.Execute(@"UPDATE Players SET Timestamp = @Timestamp WHERE Id = @Id AND Timestamp < @Timestamp", sqlParams);
                }
            }
        }

        private static void SpecifyTimestampKind(List<Player> players)
        {
            foreach (var player in players)
            {
                if (player.Timestamp.Kind == DateTimeKind.Unspecified)
                {
                    player.Timestamp = DateTime.SpecifyKind(player.Timestamp, DateTimeKind.Utc);
                }
            }
        }

        private static bool IsPrimaryKeyViolation(SqlException e)
        {
            const int PrimaryKeyViolationErrorCode = 2627;
            return e.Errors.OfType<SqlError>().Any(err => err.Number == PrimaryKeyViolationErrorCode);
        }
    }
}

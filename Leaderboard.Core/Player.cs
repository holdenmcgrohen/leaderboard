﻿// <copyright file="Player.cs" company="Leaderboard Company">
//     Copyright © Leaderboard Company 2016
// </copyright>

namespace Leaderboard.Core
{
    using System;

    /// <summary>
    /// Represents the player data in the repository
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        public long Score { get; set; }

        /// <summary>
        /// Gets or sets the time when the player data was last updated.
        /// </summary>
        public DateTime Timestamp { get; set; }
    }
}

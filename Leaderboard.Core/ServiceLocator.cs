﻿// <copyright file="ServiceLocator.cs" company="Leaderboard Company">
//     Copyright © Leaderboard Company 2016
// </copyright>

namespace Leaderboard.Core
{
    using Castle.Windsor;
    using Castle.Windsor.Configuration.Interpreters;

    /// <summary>
    /// Service locator implementation.
    /// </summary>
    public static class ServiceLocator
    {
        private static WindsorContainer container = new WindsorContainer(new XmlInterpreter());

        /// <summary>
        /// Gets the service instance.
        /// </summary>
        /// <typeparam name="T">The service type</typeparam>
        /// <returns>The service instance.</returns>
        public static T GetInstance<T>()
        {
            return container.Resolve<T>();
        }
    }
}

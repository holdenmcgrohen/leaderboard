﻿// <copyright file="IRepository.cs" company="Leaderboard Company">
//     Copyright © Leaderboard Company 2016
// </copyright>

namespace Leaderboard.Core
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Interface IRepository
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Updates the player score.
        /// </summary>
        /// <param name="playerId">The player identifier.</param>
        /// <param name="score">The score.</param>
        /// <param name="updatedDateTime">The time when the score was updated.</param>
        void UpdatePlayerScore(long playerId, long score, DateTime updatedDateTime);

        /// <summary>
        /// Gets the top players.
        /// </summary>
        /// <returns>ICollection&lt;Player&gt;.</returns>
        ICollection<Player> GetTopPlayers();
    }
}

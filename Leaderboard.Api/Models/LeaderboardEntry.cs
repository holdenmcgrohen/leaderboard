﻿// <copyright file="LeaderboardEntry.cs" company="Leaderboard Company">
//     Copyright © Leaderboard Company 2016
// </copyright>

namespace Leaderboard.Api.Models
{
    using System;
    using Core;

    /// <summary>
    /// Player data displayed on the leaderboard.
    /// </summary>
    public class LeaderboardEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeaderboardEntry"/> class.
        /// </summary>
        /// <param name="player">The player.</param>
        public LeaderboardEntry(Player player)
        {
            if (player == null)
            {
                throw new ArgumentNullException(nameof(player));
            }

            this.PlayerId = player.Id;
            this.Balance = player.Score;
            this.LastUpdateDate = player.Timestamp;
        }

        /// <summary>
        /// Gets the player balance.
        /// </summary>
        public long Balance { get; }

        /// <summary>
        /// Gets the last update date.
        /// </summary>
        public DateTime LastUpdateDate { get; }

        /// <summary>
        /// Gets the player identifier.
        /// </summary>
        public long PlayerId { get; }
    }
}

﻿// <copyright file="GameResultsCollection.cs" company="Leaderboard Company">
//     Copyright © Leaderboard Company 2016
// </copyright>

namespace Leaderboard.Api.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// A collection of game results accepted by the API.
    /// </summary>
    public class GameResultsCollection
    {
        /// <summary>
        /// Gets or sets the game results.
        /// </summary>
        public ICollection<GameResult> GameResults { get; set; }
    }
}

﻿// <copyright file="LeaderboardResult.cs" company="Leaderboard Company">
//     Copyright © Leaderboard Company 2016
// </copyright>

namespace Leaderboard.Api.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Core;

    /// <summary>
    /// The leaderboard data returned by the API.
    /// </summary>
    public class LeaderboardResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeaderboardResult"/> class.
        /// </summary>
        /// <param name="players">Player data to display on the leaderboard</param>
        public LeaderboardResult(ICollection<Player> players)
        {
            if (players == null)
            {
                throw new ArgumentNullException(nameof(players));
            }

            this.Leaderboard = players.Select(p => new LeaderboardEntry(p)).ToList();
        }

        /// <summary>
        /// Gets the players on the leaderboard.
        /// </summary>
        public ICollection<LeaderboardEntry> Leaderboard { get; }
    }
}

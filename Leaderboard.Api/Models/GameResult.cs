﻿// <copyright file="GameResult.cs" company="Leaderboard Company">
//     Copyright © Leaderboard Company 2016
// </copyright>

namespace Leaderboard.Api.Models
{
    using System;

    /// <summary>
    /// Game result accepted by the API.
    /// </summary>
    public class GameResult
    {
        /// <summary>
        /// Gets or sets the player identifier.
        /// </summary>
        public long PlayerId { get; set; }

        /// <summary>
        /// Gets or sets the game identifier.
        /// </summary>
        public long GameId { get; set; }

        /// <summary>
        /// Gets or sets the number of points won by the player.
        /// </summary>
        public long Win { get; set; }

        /// <summary>
        /// Gets or sets the date and time of the game.
        /// </summary>
        public DateTime Timestamp { get; set; }
    }
}

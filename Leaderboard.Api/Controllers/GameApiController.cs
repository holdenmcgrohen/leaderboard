﻿// <copyright file="GameApiController.cs" company="Leaderboard Company">
//     Copyright © Leaderboard Company 2016
// </copyright>

namespace Leaderboard.Api.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Http;
    using Core;
    using Models;

    /// <summary>
    /// Class GameApiController.
    /// </summary>
    public class GameApiController : ApiController
    {
        private readonly IRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameApiController"/> class.
        /// </summary>
        public GameApiController()
            : this(ServiceLocator.GetInstance<IRepository>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameApiController"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public GameApiController(IRepository repository)
        {
            if (repository == null)
            {
                throw new ArgumentNullException(nameof(repository));
            }

            this.repository = repository;
        }

        /// <summary>
        /// Gets the leaderboard data.
        /// </summary>
        /// <returns>IHttpActionResult.</returns>
        [HttpGet]
        [Route("~/api/leaderboard")]
        public IHttpActionResult Leaderboard()
        {
            var players = this.repository.GetTopPlayers();
            return this.Ok(new LeaderboardResult(players));
        }

        /// <summary>
        /// Updates game results.
        /// </summary>
        /// <param name="results">The results.</param>
        /// <returns>IHttpActionResult.</returns>
        [HttpPost]
        [Route("~/api/gameResults")]
        public IHttpActionResult UpdateGameResults([FromBody]GameResultsCollection results)
        {
            var resultsByPlayer =
                results.GameResults
                .GroupBy(r => r.PlayerId)
                .Select(
                    g => new
                    {
                        PlayerId = g.Key,
                        Score = g.Sum(x => x.Win),
                        UpdatedDateTime = g.Max(x => x.Timestamp)
                    });

            foreach (var playerResult in resultsByPlayer)
            {
                this.repository.UpdatePlayerScore(playerResult.PlayerId, playerResult.Score, playerResult.UpdatedDateTime);
            }

            return this.Ok();
        }
    }
}

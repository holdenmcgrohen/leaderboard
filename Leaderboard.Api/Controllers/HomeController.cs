﻿// <copyright file="HomeController.cs" company="Leaderboard Company">
//     Copyright © Leaderboard Company 2016
// </copyright>

namespace Leaderboard.Api.Controllers
{
	using System.Web.Mvc;

	/// <summary>
	/// Class HomeController.
	/// </summary>
	public class HomeController : Controller
    {
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns>System.String.</returns>
        public string Index()
        {
            return "Leaderboard API here";
        }
    }
}

﻿// <copyright file="TestRepository.cs" company="Leaderboard Company">
//     Copyright © Leaderboard Company 2016
// </copyright>

namespace Leaderboard.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Core;

    /// <summary>
    /// Test repository that stores data in memory.
    /// </summary>
    public class TestRepository : IRepository
    {
        private readonly List<Player> players = new List<Player>();

        /// <inheritdoc/>
        public ICollection<Player> GetTopPlayers()
        {
            return this.players.OrderByDescending(p => p.Score).ToList();
        }

        /// <inheritdoc/>
        public void UpdatePlayerScore(long playerId, long score, DateTime updatedDateTime)
        {
            var player = this.players.FirstOrDefault(p => p.Id == playerId);
            if (player == null)
            {
                player = new Player { Id = playerId };
                this.players.Add(player);
            }

            player.Score += score;
            player.Timestamp = updatedDateTime;
        }
    }
}

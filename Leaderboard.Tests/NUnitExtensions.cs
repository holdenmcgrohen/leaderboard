﻿// <copyright file="NUnitExtensions.cs" company="Leaderboard Company">
//     Copyright © Leaderboard Company 2016
// </copyright>

namespace Leaderboard.Tests
{
    using System;
    using NUnit.Framework;

    /// <summary>
    /// Class NUnitExtensions.
    /// </summary>
    internal static class NUnitExtensions
    {
        /// <summary>
        /// Checks that the actual value is equal to the expected one.
        /// </summary>
        /// <param name="actual">The actual value.</param>
        /// <param name="expected">The expected value.</param>
        /// <returns>System.Object.</returns>
        internal static object ShouldEqual(this object actual, object expected)
        {
            Assert.AreEqual(expected, actual);
            return expected;
        }

		/// <summary>
		/// Checks that the actual object is of the specified type.
		/// </summary>
		/// <param name="actual">The actual object.</param>
		/// <param name="expected">The expected type.</param>
		internal static void ShouldBeOfType(this object actual, Type expected)
        {
            Assert.IsInstanceOf(expected, actual);
        }

		/// <summary>
		/// Checks that the actual object isnot null.
		/// </summary>
		/// <param name="anObject">The actual object.</param>
		internal static void ShouldNotBeNull(this object anObject)
        {
            Assert.IsNotNull(anObject);
        }
    }
}

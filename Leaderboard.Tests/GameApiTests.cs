﻿// <copyright file="GameApiTests.cs" company="Leaderboard Company">
//     Copyright © Leaderboard Company 2016
// </copyright>

namespace Leaderboard.Tests
{
    using System;
    using System.Linq;
    using Api.Controllers;
    using Api.Models;
    using NUnit.Framework;

    /// <summary>
    /// Class GameApiTests.
    /// </summary>
    [TestFixture]
    public class GameApiTests
    {
        /// <summary>
        /// Determines whether the API can return empty leaderboard.
        /// </summary>
        [Test]
        public static void CanGetEmptyLeaderboard()
        {
            using (var controller = new GameApiController(new TestRepository()))
            {
                var players = controller.GetLeaderboard();
                players.Count.ShouldEqual(0);
            }
        }

        /// <summary>
        /// Checks whether the API can update results.
        /// </summary>
        [Test]
        public static void CanUpdateResults()
        {
            using (var controller = new GameApiController(new TestRepository()))
            {
                var date = DateTime.Now;
                var result = new GameResult { PlayerId = 1, GameId = 10, Win = 100, Timestamp = date };
                controller.UpdateResults(new[] { result });

                var players = controller.GetLeaderboard();
                players.Count.ShouldEqual(1);
                var player = players.Single();
                player.PlayerId.ShouldEqual(1);
                player.Balance.ShouldEqual(100);
                player.LastUpdateDate.ShouldEqual(date);
            }
        }

        /// <summary>
        /// Checks whether the API can aggregate results.
        /// </summary>
        [Test]
        public static void CanAggregateGameResults()
        {
            using (var controller = new GameApiController(new TestRepository()))
            {
                var results = new[]
                {
                    new GameResult { PlayerId = 1, GameId = 10, Win = 100, Timestamp = DateTime.Now },
                    new GameResult { PlayerId = 1, GameId = 10, Win = 100, Timestamp = DateTime.Now }
                };
                controller.UpdateResults(results);

                var players = controller.GetLeaderboard();
                players.Single().Balance.ShouldEqual(200);
            }
        }

        /// <summary>
        /// Checks whether the API can display sorted game results.
        /// </summary>
        [Test]
        public static void CanSortGameResults()
        {
            using (var controller = new GameApiController(new TestRepository()))
            {
                var results = new[]
                {
                    new GameResult { PlayerId = 1, GameId = 10, Win = 100, Timestamp = DateTime.Now },
                    new GameResult { PlayerId = 2, GameId = 10, Win = 200, Timestamp = DateTime.Now }
                };
                controller.UpdateResults(results);

                var players = controller.GetLeaderboard();
                players.First().PlayerId.ShouldEqual(2);
                players.Last().PlayerId.ShouldEqual(1);
            }
        }
    }
}

﻿// <copyright file="GameApiControllerExtensions.cs" company="Leaderboard Company">
//     Copyright © Leaderboard Company 2016
// </copyright>

namespace Leaderboard.Tests
{
    using System.Collections.Generic;
    using System.Web.Http.Results;
    using Api.Controllers;
    using Api.Models;
    using Core;

    /// <summary>
    /// Class GameApiControllerExtensions.
    /// </summary>
    internal static class GameApiControllerExtensions
    {
        /// <summary>
        /// Calls the Leaderboard method.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <returns>ICollection&lt;Player&gt;.</returns>
        internal static ICollection<LeaderboardEntry> GetLeaderboard(this GameApiController controller)
        {
            var result = controller.Leaderboard();
            result.ShouldBeOfType(typeof(OkNegotiatedContentResult<LeaderboardResult>));
            var entries = ((OkNegotiatedContentResult<LeaderboardResult>)result).Content.Leaderboard;
            entries.ShouldNotBeNull();
            return entries;
        }

        /// <summary>
        /// Calls the UpdateGameResults method.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="results">The results.</param>
        internal static void UpdateResults(this GameApiController controller, ICollection<GameResult> results)
        {
            var result = controller.UpdateGameResults(new GameResultsCollection { GameResults = results });
            result.ShouldBeOfType(typeof(OkResult));
        }
    }
}

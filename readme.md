﻿# Leader-board sample task

## Context
We are working for an online gaming company which operates many game servers. Players constantly 
play games on those servers and each game played results into a win or loss of points for the 
player. This data is maintained in memory by each game server, and periodically (e.g. every 10-­20 
seconds) each server needs to persist this data.

## The task
We need to implement a web service that exposes the following 2 endpoints:

**Endpoint 1.**
An endpoint that allows the game servers to periodically persist their data:  `POST api/gameResults` 
, accepting the following JSON input:
```
{
"gameResults": [
{ "playerId": 1, "gameId": 34, "win": 100, "timestamp": "2016‐03‐21T10:30:12Z" },
{ "playerId": 2, "gameId": 55, "win": ‐100, "timestamp": "2016‐03‐21T10:30:13Z" },
{ "playerId": 1, "gameId": 55, "win": 200, "timestamp": "2016‐03‐21T10:31:13Z" },
]
}
```

*Description:*
`playerId (long)` ­ — id of the player
`gameId (long)` — id of the game played by the player
`win (long)` — the player's points win amount (positive for a win, negative for a loss) 
`timestamp (date time)`  — date time when game was played (in UTC)

As a result of the call to this endpoint the players' points balances must at some point be updated 
in the database. If a player does not have a balance already in the database, one needs to be 
created.

*NOTES:*
One batch of data might contain several records for a single player (i.e. player played several 
games).
There are multiple game servers, serving different games, and so the service will get concurrent 
requests, which also might contain game results for the same player.
Initially this service runs as a pilot project of a single server. Therefore data loss due to 
server or service malfunction is not considered critical, but should not happen under normal 
circumstances.

**Endpoint 2.**
An endpoint that allows the web sites where the players start the games to display a "leader-board". 
A "leader-board" is the top 100 players sorted in descending order by their points balance. The 
endpoint responds to  `GET api/leaderboard` , takes no parameters and returns data in the following 
JSON format:
```
{
"leaderboard": [
{ "playerId": 1, "balance": 300, "lastUpdateDate": "2016‐03‐21T10:31:13Z"},
{ "playerId": 2, "balance": ‐100, "lastUpdateDate": "2016‐03‐21T10:30:13Z"}
]
}
```
Description:

`playerId (long)` — id of the player
`balance (long)` — player's current points balance
`lastUpdateDate (date time)` — date time when player's balance was last updated (in the time zone of 
the application server)

*NOTE:*
The players are very competitive, and there are many concurrent players, so the leader board 
endpoint will be called very often (thousands requests per minute).


## The solution
**Technologies and frameworks used:**
1) ASP.NET WebAPI — provides the means to handle HTTP requests and easily serialize/deserialize data
2) SQL Server database — for storing data and ensuring data integrity (`InitializeDatabase.sql` script included)
3) Dapper — high-performance micro-ORM for accessing DB data
4) Castle Windsor — a IoC container for decoupling components and resolving component dependencies and settings at runtime

**Startup project:** `Leaderboard.Api`

**Unit test project:** `Leaderboard.Tests`

**Configuration & launch steps:**
1) Use `InitializeDatabase.sql` to create a database
2) Modify the DB connection string in web.config file, section `configuration/castle/components/component id="Repository"/parameters/connectionString`
3) Launch `Leaderboard.Api` project

**Performance and scalability notes:**
— The application is designed in such a way that all concurrency issues are controlled by the DB mechanisms.
  The choice of creating a new record or updating the exisiting the is made by checking a unique key constraint violation — a fast and reliable procedure.
  This means that the service can be scaled horizontally without any modifications.
— There are no full-table locks, no transactions with high isolation levels, so concurrent DB requests have as little impact on each other as possible.
— The fast performance of leaderboard DB requests is ensured by a column index.

